Title: Adhésion
Slug: adhesions
Authors: AFPy
Summary: Adhésion


### Pourquoi Adhérer ?

#### Adhérer pour soutenir les actions de l'AFPy

* Organiser les PyConFR
* Traduire la documentation
* Organiser des MeetUp
* Développer la platerforme HackInScience
* Administrer le forum
* Héberger une liste d’offre emploi liés à Python
* Être membre de l’April pour soutenir le logiciel libre

#### Ce que permet l'adhésion à l'AFPy

* Participer au vote à l’Assemblée Générale
* Pouvoir devenir membre du Comité Directeur
* Utiliser le BigBlueButton de l’association
* Avoir le badge Membre sur Discourse

#### Ce qu'on aimerait faire aussi

* Des ateliers python dans les lycées
* Mettre en place une certification Python
* Être présents aux évènements
* Contribuer au Python libre
* Rémunérer des gens faisant vivre l’association


### Adhérez à l'AFPy

<iframe id="haWidget" src="https://www.helloasso.com/associations/afpy/adhesions/adhesion-2023-a-l-afpy/widget"></iframe>

Si le widget ne fonctionne pas, essayez cette page : [https://www.helloasso.com/associations/afpy/adhesions/adhesion-2023-a-l-afpy](https://www.helloasso.com/associations/afpy/adhesions/adhesion-2023-a-l-afpy)
