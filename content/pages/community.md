Title: Communauté
Slug: communaute
Authors: AFPy
Summary: Communauté


### Forum de discussion

Afin d'échanger avec la communauté, un [forum de discussion](https://discuss.afpy.org/) est disponible et traite de tous les sujets autour de Python.


### Salons de discussion

L'AFPy est présente sur [IRC](https://www.afpy.org/irc) et [Discord](https://www.afpy.org/discord).


### Rencontres

Afin de partager autour du langage Python, de ses pratiques, de sa technique et de son
écosystème, des évènements sont organisés régulièrement dans divers lieux.

* [Amiens](https://www.meetup.com/fr-FR/Python-Amiens)
* [Bordeaux](https://www.meetup.com/fr-FR/py-bdx/)
* [Bruxelles](https://www.meetup.com/fr-FR/Belgium-Python-Meetup-aka-AperoPythonBe/)
* [Grenoble](https://www.meetup.com/fr-FR/Groupe-dutilisateurs-Python-Grenoble/)
* [Lille](https://www.meetup.com/fr-FR/Lille-py/)
* [Lyon](https://www.meetup.com/fr-FR/Python-AFPY-Lyon/)
* [Nantes](https://www.meetup.com/fr-FR/Nantes-Python-Meetup/)
* [Montpellier](https://www.meetup.com/fr-FR/Meetup-Python-Montpellier/)
* [Rennes](https://www.meetup.com/fr-FR/python-rennes/)


### PyConFR

La [PyConFR](https://www.pycon.fr/) est un évènement organisé chaque année depuis 10+ ans par l'AFPy. Cette conférence est gratuite, entièrement organisée par des bénévoles et regroupe développeu·ses·rs, chercheu·ses·rs, étudiant·e·s et amat·rices·eurs autour d'une même passion pour le langage de programmation Python.


### April

Pionnière du logiciel libre en France, l'[April](https://april.org/campagne/) est depuis 1996 un acteur majeur de la démocratisation et de la diffusion du logiciel libre et des standards ouverts auprès du grand public, des professionnels et des institutions dans l'espace francophone.
