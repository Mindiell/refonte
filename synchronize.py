# encoding: utf-8

from datetime import datetime
import os
from time import mktime

import feedparser


JOB_URL = "https://discuss.afpy.org/c/emplois/14.rss"
NEWS_URL = "https://discuss.afpy.org/c/association/actualites/16.rss"


def synchronize_jobs():
    """
    Get jobs data from AFPy website and parse it to update jobs.
    """
    os.makedirs("content/jobs", exist_ok=True)
    data = feedparser.parse(JOB_URL)
    for entry in data.entries:
        # Retrieve informations
        job_id = entry.id.split("-")[-1]
        job_title = entry.title
        job_author = entry.author
        job_link = entry.link
        job_date = datetime.fromtimestamp(mktime(entry.published_parsed))
        job_body = ""
        for line in entry.description.splitlines()[:-2]:
            job_body += line.strip() + "\n"
        job_body += "\n"
        job_body += f"[Voir l'article original]({job_link})\n"

        # Generating filename to save/replace job offer
        filename = f"content/jobs/offre_{job_id}.md"
        # Writing file
        with open(filename, "w") as file_handle:
            file_handle.write(f"Title: {job_title}\n")
            file_handle.write(f"Date: {job_date}\n")
            file_handle.write(f"Category: jobs\n")
            file_handle.write(f"Slug: offre-{job_id}\n")
            file_handle.write(f"Author: {job_author}\n")
            file_handle.write(f"\n")
            file_handle.write(f"\n")
            file_handle.write(f"{job_body}")


def synchronize_news():
    """
    Get news data from AFPy website and parse it to update jobs.
    """
    os.makedirs("content/news", exist_ok=True)
    data = feedparser.parse(NEWS_URL)
    for entry in data.entries:
        # Retrieve informations
        news_id = entry.id.split("-")[-1]
        news_title = entry.title
        news_author = entry.author
        news_link = entry.link
        news_date = datetime.fromtimestamp(mktime(entry.published_parsed))
        news_body = ""
        for line in entry.description.splitlines()[:-2]:
            news_body += line.strip() + "\n"
        news_body += "\n"
        news_body += f"[Voir l'article original]({news_link})\n"

        # Generating filename to save/replace job offer
        filename = f"content/news/actu_{news_id}.md"
        # Writing file
        with open(filename, "w") as file_handle:
            file_handle.write(f"Title: {news_title}\n")
            file_handle.write(f"Date: {news_date}\n")
            file_handle.write(f"Category: news\n")
            file_handle.write(f"Slug: actu-{news_id}\n")
            file_handle.write(f"Author: {news_author}\n")
            file_handle.write(f"\n")
            file_handle.write(f"\n")
            file_handle.write(f"{news_body}")


if __name__ == "__main__":
    synchronize_jobs()
    synchronize_news()
