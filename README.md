# AFPy.org

Site Web de l'AFPy.


## Lancer localement

Commencez par un `make install`.

Puis un `make serve` suffit pour jouer localement.


## Publier

```bash
make publish
```
